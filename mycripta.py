from random import randint
from math import gcd

def cifrar(m, e, n):
    """Encripta mensaje m """
    return (m**e)%n

def descifrar(c, d, n):
    """Desecripta mensaje c"""
    return (c**d)%n

def gen_pq(size_row, size_col):
    """Elije 2 numeros primos al azar de entre los 1ros 10mil

        size_row: fila tope del archivo donde buscar
        size_col: columna tope del archivo donde buscar
    """
    primes_file = open('primes_db/10000.csv')
    row = 1
    # 1st number row and col
    n1_row = randint(1,size_row)
    n1_col = randint(1,size_col)
    # 2nd number row and col
    n2_row = randint(1,1000)
    n2_col = randint(1,10)

    p = q = False
    for i in primes_file:
        if row == n1_row:
            p = int(i.split(',')[n1_col-1])
        if row == n2_row:
            q = int(i.split(',')[n2_col-1])

        if p and q:
            break
        row += 1
    return p, q

def mymcd(a, b):
    """Retorna el maximo comun divisor de a y b

       Implementado en forma recursiva. No sirve para este caso x lento.
    """
    if a > b:
        return mymcd(a - b, b)
    elif a < b:
        return mymcd(a, b - a)
    else:
        return a

def choose_e(phi):
    """Calculo el numero e.

    Number e must be:
        gcd(e,phi) = 1
        1 < e < phi
    """
    e = 2;
    while (e < phi):
        if gcd(e, phi) == 1:
            return e
        e += 1
def choose_d(phi, e):
    """ Calculo el numero d

    Number e must be:
        (d * e - 1) is divisible by phi
    """
    for d in range(3, phi, 2):
        if ((d*e)-1) % phi == 0:
            return d

def main():
    # busco 2 numeros primos
    p, q = gen_pq(8, 9)

    # modulo para cifrado y descifrado
    n = p * q

    # totalizador de euler
    phi = (p - 1) * (q - 1)
    e = choose_e(phi)
    d = choose_d(phi, e)

    print("Public key [e,n]: ", e, n)
    print("Private key [d,n]: ", d, n)

    # mensaje a encriptar
    m = input("Ingrese numero entero a cifrar: ")
    mensaje_cifrado = []
    for letra in m:
        mensaje_cifrado.append(cifrar(ord(letra), e, n))
        #c = cifrar(m, e, n)
    print("Mensaje cifrado: ", mensaje_cifrado)
    mensaje_descifrado = []
    for nc in mensaje_cifrado:
        mensaje_descifrado.append(chr(descifrar(nc, d, n)))
    print("Mensaje descifrado: ", ''.join(mensaje_descifrado))

if __name__ == '__main__':
    main()
