#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 13:08:54 2017

@author: emiliano
"""

class Cifrador():
    def __init__(self, texto = None):
        self.texto = texto.upper()
    
    def cifrar(self):
        texto_cifrado = ""
        for letra in self.texto:
            texto_cifrado += str(ord(letra))
        self.texto_cifrado = texto_cifrado
    
    def descifrar(self):
        texto_descifrado = ""
        for i in range(0,len(self.texto_cifrado),2):
            texto_descifrado += chr(int(self.texto_cifrado[i]+self.texto_cifrado[i+1]))
        self.texto_descifrado = texto_descifrado