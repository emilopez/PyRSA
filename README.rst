PyRSA
=====

An implementation of RSA public key encryption algorithms in python, this implementation is for educational purpose, and is not intended for real world use.

Programa Python para encriptar y desencriptar mensajes utilizando el algoritmo de cifrado de clave pública RSA. Algunas consideraciones:

- El texto nativo M y el texto cifrado C son enteros entre *0* y *n-1* para algún *n* (n es el producto de dos primos)
        - C = (M^e) mod n
        - M = (C^d) mod n

- Emisor y receptor deben conocer *n*.
- El emisor conoce el valor de *e*
- El receptor **SOLO** debe conocer el valor de *d*

Sobre el algoritmo
------------------

- Seleccionar dos números primos *p* y *q*
- Calcular n = p * q
- Calcular phi = (p-1)*(q-1)
- Buscar un *e* tal que:
    - gcd(phi, e) = 1; => significa que phi y e son primos relativos
    - 1 < e < phi
- Buscar un *d* tal que: (d * e) - 1 sea divisible por phi

Sobre el código del programa
----------------------------
Este programa fue desarrollado con fines didácticos y para despuntar el vicio, por lo que se buscó mantener la claridad en detrimento de la velocidad de cálculo (no se encuentra optimizado).

Modo de uso
'''''''''''
se ejecuta el script y se ingresa un valor entero. El programa lo encripta y desencripta usando RSA.

Comentarios de implementación
''''''''''''''''''''''''''''''
- Los números primos no son generados, sino que se leen en forma aleatoria del archivo primes_db/10000.csv (obtenido de http://primes.utm.edu/)
- Para corroborar que el máximo común divisor entre e y phi sea 1, se utiliza la función gcd del módulo fractions, aunque en el código está programada la versión recursiva e inutil de la misma (para refrescar la memoria)

Bibliografía
''''''''''''
Esta basado en lo descripto en el capítulo 18.4 "Cifrado de Clave Pública y Firmas Digitales" (pag. 624) del libro "Comunicaciones y Redes de Computadores" de William Stallings 6ta edición.
