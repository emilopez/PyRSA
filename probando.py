#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 12:32:37 2017
@author: emiliano
"""
#def cifrador(texto, clave):
#    texto = texto.upper()
#    texto_cifrado = ""
#    for letra in texto:
#        texto_cifrado += str(ord(letra)-clave)
#    return texto_cifrado
#
#def descifrador(cifrado, clave):
#    texto_descifrado = ""
#    for i in range(0,len(cifrado),2):
#        texto_descifrado += chr(clave+int(cifrado[i]+cifrado[i+1]))
#    return texto_descifrado
#        
#texto = input("Ingrese texto: ")
#cifrado = cifrador(texto, 10)
#print(cifrado)
#texto2 = descifrador(cifrado,10)
#print(texto2)

from cifrador import Cifrador

c1 = Cifrador("texto de prueba")
c1.cifrar()
print(c1.texto_cifrado)
c1.descifrar()
print(c1.texto_descifrado)

c2 = Cifrador("otro textito")
c2.cifrar()
print(c2.texto_cifrado)

print(c1.texto_descifrado)

lista = [c1, c2]